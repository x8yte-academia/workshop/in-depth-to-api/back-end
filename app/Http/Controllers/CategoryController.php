<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Transformers\CategoryTransformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use Helpers;

    protected $resourceKey = 'category';

    public function index()
    {
        $orm = Category::all();

        return $this->collection($orm, new CategoryTransformer, [
            'key' => $this->resourceKey,
        ]);
    }

    public function show($id)
    {
        $orm = Category::find($id);

        if ($orm) {
            return $this->item($orm, new CategoryTransformer, [
                'key' => $this->resourceKey,
            ]);
        }

        return $this->response->errorNotFound();
    }

    public function store(Request $request)
    {
        $orm = new Category([
            'name' => $request->input($this->resourceKey)['categoryName'],
        ]);

        try {
            $orm->save();
        } catch (Exception $e) {
            return $this->response->internalError();
        }

        return $this->item($orm, new CategoryTransformer, [
            'key' => $this->resourceKey,
        ]);
    }

    public function update(Request $request, $id)
    {
        $orm = Category::find($id);

        if ($orm) {
            $orm->fill([
                'name' => $request->input($this->resourceKey)['categoryName'],
            ]);

            $orm->save();

            return $this->item($orm, new CategoryTransformer, [
                'key' => $this->resourceKey,
            ]);
        }

        return $this->response->errorNotFound();
    }

    public function destroy($id)
    {
        $orm = Category::find($id);

        if ($orm) {
            $orm->delete();

            return $this->response->noContent();
        }

        return $this->response->errorNotFound();
    }
}
