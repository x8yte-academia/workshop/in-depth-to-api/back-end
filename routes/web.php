<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$api = app('Dingo\Api\Routing\Router');
$namespace = 'App\Http\Controllers\\';

$api->version('v1', function ($api) use ($namespace) {
    // Categories Resource
    $api->get('categories', $namespace . 'CategoryController@index');
    $api->get('categories/{id}', $namespace . 'CategoryController@show');
    $api->post('categories', $namespace . 'CategoryController@store');
    $api->patch('categories/{id}', $namespace . 'CategoryController@update');
    $api->delete('categories/{id}', $namespace . 'CategoryController@destroy');
});
