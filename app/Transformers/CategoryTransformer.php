<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Category;

class CategoryTransformer extends TransformerAbstract
{

    protected $availableIncludes = [];

    protected $defaultIncludes = [];

    /**
     * Transform the fields
     */
    public function transform(Category $field)
    {
        return [
            'id' => $field->id,
            'categoryName' => $field->name,
        ];
    }
}
